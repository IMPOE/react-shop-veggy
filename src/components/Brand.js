import React from 'react'

const Brand = () => {
    return (
      <div className="brand">
        <img
          className="logo"
          src="https://res.cloudinary.com/sivadass/image/upload/v1493547373/dummy-logo/Veggy.png"
          alt="Veggy Brand Logo"
        />
      </div>
    );
}

export default Brand;