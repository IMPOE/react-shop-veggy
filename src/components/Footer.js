import React from "react";

const Footer = () => {
  return (
    <footer>
      <p className="footer-links">
        <a
          href="https://github.com/Ramzes-org/Veggy"
          target="_blank"
        >
          View Source on Github
        </a>
        <span> / </span>
        <a href="mailto:0968512988z@gmail.com" target="_blank">
          Need any help?
        </a>
        <span> / </span>
        <a href="https://twitter.com/Ramzes9112" target="_blank">
          Say Hi on Twitter
        </a>
        <span> / </span>
        <a href="#" >
          Read My Blog
        </a>
      </p>
      <p>
        &copy; 2018 <strong>Veggy</strong> - Organic Green Store
      </p>
    </footer>
  );
};

export default Footer;
